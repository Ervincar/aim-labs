from collections import Counter
from csv import reader, writer
from datetime import date
from pathlib import Path
from random import choice, randint

import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from mimesis import Code, Person
from mimesis.enums import Gender, Locale


ROW_COUNT_FROM = 1000
ROW_COUNT_TO = 3000

PERSON_AGE_FROM = 18
PERSON_AGE_TO = 65
EMPLOYMENT_AGE_FROM = 18
EMPLOYMENT_AGE_TO = 55
SALARY_FROM = 16000
SALARY_TO = 340000
COMPLETED_PROJECTS_FROM = 0
COMPLETED_PROJECTS_TO = 100

CSV_FILE_PATH = Path(__file__).parent.resolve() / "company.csv"

COMPANY_STRUCTURE = {
    "IT": ["Senior", "Middle", "Junior"],
    "Маркетинг": ["Управляющий отделом", "Менеджер", "Работник"],
    "Тестирование": ["Управляющий отделом", "Senior тестировщик", "Middle тестировщик"],
    "Развитие": ["Директор по развитию", "Специалист", "Работник"]
}


person = Person(Locale.RU)
code = Code()


def generate_data(rows: int):
    current_year = date.today().year

    for _ in range(rows):
        gender = choice([Gender.MALE, Gender.FEMALE])
        birth_year = current_year - randint(PERSON_AGE_FROM, PERSON_AGE_TO)
        department = choice(list(COMPANY_STRUCTURE))

        yield [
            person.identifier(),
            gender.value,
            f"{person.surname(gender)} {person.name(gender)[0]}. {person.name(Gender.MALE)[0]}.",
            birth_year,
            min(current_year, birth_year + randint(EMPLOYMENT_AGE_FROM, EMPLOYMENT_AGE_TO)),
            department,
            np.random.choice(COMPANY_STRUCTURE[department], 1, False, [0.1, 0.3, 0.6])[0],
            randint(SALARY_FROM, SALARY_TO),
            randint(COMPLETED_PROJECTS_FROM, COMPLETED_PROJECTS_TO),
        ]


def np_analyze():
    with open(CSV_FILE_PATH, encoding="utf-8") as csv_file:
        data_raw = [list(row) for row in reader(csv_file)]

    data = np.array(data_raw)
    genders = data[:, 1]
    salary = data[:, 3].astype("float64")
    birth_year = data[:, 3].astype("int32")

    print("NP")
    print("Доля сотрудников женского пола:", np.sum(genders == "female") / np.size(genders))
    print("Минимальная з/п:", np.min(salary))
    print("Максимальная з/п:", np.max(salary))
    print("Средняя з/п:", np.average(salary))
    print("Дисперсия з/п:", np.var(salary))
    print("Ст. откл. года рождения:", np.std(birth_year))
    print("Медиана года рождения:", np.median(birth_year))
    print("Мода года рождения:", Counter(birth_year).most_common(1)[0][0])


def pandas_analyze():
    data = pd.read_csv(CSV_FILE_PATH, header=None)
    genders = data[1]
    salary = data[7]
    birth_year = data[3]
    print("PD")
    print("Доля сотрудников женского пола:", genders.value_counts()["female"] / genders.shape[0])
    print("Минимальная з/п:", salary.min())
    print("Максимальная з/п:", salary.max())
    print("Средняя з/п:", salary.mean())
    print("Дисперсия з/п:", salary.var())
    print("Ст. откл. года рождения:", birth_year.std())
    print("Медиана года рождения:", birth_year.median())
    print("Мода года рождения:", birth_year.mode()[0])

    plt.figure(figsize=(14, 10), dpi=80)
    plt.hlines(y=birth_year, xmin=0, xmax=salary, color="C0", alpha=0.4, linewidth=5)
    plt.gca().set(ylabel="Birth year", xlabel="Salary")
    plt.show()

    plt.pie([genders.value_counts()["male"], genders.value_counts()["female"]], labels=["Men", "Women"])
    plt.show()

    plt.figure(figsize=(16, 10), dpi=80)
    plt.plot_date(data[4], salary)
    plt.gca().xaxis.set_major_locator(mdates.AutoDateLocator())
    plt.ylabel("Salary")
    plt.xlabel("Dates")
    plt.show()


def main():
    with open(CSV_FILE_PATH, "w", encoding="utf-8") as csv_file:
        writer(csv_file, lineterminator="\n").writerows(generate_data(randint(ROW_COUNT_FROM, ROW_COUNT_TO)))

    np_analyze()
    print("\n\n")
    pandas_analyze()


if __name__ == "__main__":
    main()
