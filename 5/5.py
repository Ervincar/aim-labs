import numpy
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score


TRAIN_CSV_FILE_PATH = Path(__file__).parent.resolve() / "train.csv"
TEST_CSV_FILE_PATH = Path(__file__).parent.resolve() / "test.csv"
CATEGORIES = [
    "Gender",
    "Customer Type",
    "Type of Travel",
    "Class",
    "Inflight wifi service",
    "Departure/Arrival time convenient",
    "Ease of Online booking",
    "Gate location",
    "Food and drink",
    "Online boarding",
    "Seat comfort",
    "Inflight entertainment",
    "On-board service",
    "Leg room service",
    "Baggage handling",
    "Checkin service",
    "Inflight service",
    "Cleanliness",
]


def svc_classification(x_data, y_label, X_data, Y_label):
    svc_clf = SVC()
    svc_clf.fit(x_data, y_label)
    svc_prediction = svc_clf.predict(X_data)
    acc_score = accuracy_score(svc_prediction, Y_label)
    print(classification_report(svc_prediction, Y_label))
    print(acc_score)
    print(confusion_matrix(svc_prediction, Y_label))
    return acc_score


def naive_bayes(x_data, y_label, X_data, Y_label):
    nb = GaussianNB()
    nb.fit(x_data, y_label)
    nb_prediction = nb.predict(X_data)
    acc_score = accuracy_score(nb_prediction, Y_label)
    print(classification_report(nb_prediction, Y_label))
    print(acc_score)
    print(confusion_matrix(nb_prediction, Y_label))
    return acc_score


def mlp_classifier(x_data, y_label, X_data, Y_label):
    clf = MLPClassifier(random_state=1, max_iter=300)
    clf.fit(x_data, y_label)
    mlp_prediction = clf.predict(X_data)
    acc_score = accuracy_score(mlp_prediction, Y_label)
    print(classification_report(mlp_prediction, Y_label))
    print(acc_score)
    print(confusion_matrix(Y_label, mlp_prediction))
    return acc_score


test_dataset = pd.read_csv(TEST_CSV_FILE_PATH)
test_label = test_dataset["satisfaction"]
test_dataset.drop("satisfaction", axis=1, inplace=True)

test_dataset["Arrival Delay in Minutes"] = test_dataset["Arrival Delay in Minutes"].apply(
    lambda x: 0 if numpy.isnan(x) else x
)

test_dataset["Gender"] = test_dataset["Gender"].apply(lambda x: 1 if x == "Male" else 0)
test_dataset["Customer Type"] = test_dataset["Customer Type"].apply(lambda x: 1 if x == "Loyal customer" else 0)
test_dataset["Type of Travel"] = test_dataset["Type of Travel"].apply(lambda x: 1 if x == "Business Travel" else 0)
test_dataset["Class"] = test_dataset["Class"].apply(lambda x: 2 if x == "Business" else (1 if x == "Eco Plus" else 0))

test_dataset[CATEGORIES] = test_dataset[CATEGORIES].astype("category")

continuous_features = set(test_dataset.columns) - set(test_dataset[CATEGORIES])
scaler = StandardScaler()
data_norm = test_dataset.copy()
data_norm[list(continuous_features)] = scaler.fit_transform(test_dataset[list(continuous_features)])

X_train, X_test, y_train, y_test = train_test_split(data_norm, test_label, test_size=0.20, random_state=34)

acc_score_svc = svc_classification(X_train, y_train, X_test, y_test)
print("=======================")
acc_score_nb = naive_bayes(X_train, y_train, X_test, y_test)
print("=======================")
acc_score_mlp = mlp_classifier(X_train, y_train, X_test, y_test)

plt.bar(["svc", "nb", "mlp"], [acc_score_svc, acc_score_nb, acc_score_mlp])
plt.show()
